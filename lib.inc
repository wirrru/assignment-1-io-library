section .text


; Принимает код возврата и завершает текущий процесс
exit:
    mov  rax, 60
    xor  rdi, rdi
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
xor rax, rax
.loop:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov  rdx, rax
    mov  rsi, rdi
    mov  rax, 1
    mov  rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    sub rsp, 8
	mov rax, rdi
    dec rsp
    mov byte[rsp], 0x0
    mov rsi, 1
    mov rcx, 10
.loop:
    xor rdx, rdx
    div rcx
    add rdx, '0'
    dec rsp
    mov [rsp], dl
    inc rsi
    test rax, rax
    jz .end
    jnz .loop
.end:
    mov rdi, rsp
    push rsi
    call print_string
    pop rsi
    add rsp, rsi
    add rsp, 8
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    xor rax, rax
	test rdi, rdi
	jge .print
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
 .print:
	call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
.loop:
    mov al, [rdi+rcx]
    cmp al, [rsi+rcx]
    jne .fail
    inc rcx
    or al, al
    jnz .loop
    mov rax, 1
    ret
.fail:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push rax
    mov rsi, rsp
    mov rdx, 1
    xor rdi, rdi
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14
    xor r12, r12
    mov r13, rdi
    mov r14, rsi

.loop:
    mov rdi, r12
    call read_char
    cmp al, 0x20
    je .loop
    cmp al, 0x9
    je .loop
    cmp al, 0xA
    je .loop
    
.count:
    cmp r12, r14
    je .fail
    test al, al
    je .success
    cmp al, 0x20
    je .success
    cmp al, 0x9
    je .success
    cmp al, 0xA
    je .success
    mov byte [r12 + r13], al
    inc r12
    mov rdi, r12
    call read_char
    jmp .count

.success:
    mov byte [r12 + r13], 0
    mov rdx, r12
    mov rax, r13
    pop r14
    pop r13
    pop r12
    ret
.fail:
    xor rax, rax
    pop r14
    pop r13
    pop r12
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
	xor rdx, rdx
.loop:
	cmp byte[rdi+rdx], '0'
	jl .end
	cmp byte[rdi+rdx], '9'
	jg .end
	push rdx
	mov rdx, 10
	mul rdx
	pop rdx
	xor r11, r11
	mov r11b, byte[rdi+rdx]
	add rax, r11
	sub rax, '0'
	inc rdx
	jmp .loop
.end:
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    push rbx
.loop:
    cmp rax, rdx
    je .buffer_full
    mov bl, byte[rdi + rax]
    mov byte[rsi + rax], bl
    test rbx, rbx
    je .end
    inc rax
    jmp .loop
.buffer_full:
    xor rax, rax
.end:
    pop rbx
    ret
